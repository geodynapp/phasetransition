# Software related with Regime Transitions in Structured Populations


This repository of the software used in *Regime Transitions in Structured Populations* to compute the average fixation probability of graphs.

There are three main files depending on the kind of computation you are going to make.

1. **main.cpp** symbolically computes the transition matrices and computes the coefficients of the rational function giving the average fixation probability. It is based on IDs and graph automorphisms lists (optional).

1. **main_ln22.cpp** started to compute the evolutionary behavior of larger graphs, now can compute the average fixation function for graphs which *know how* are their symmetries. Some of these graphs have been implemented. You need to recompile each time you change of graph. Essentially works like previous program.

1. **main_bp.cpp**. In this case we use the [Barreras & Peña solver check](https://doi.org/10.1007/s10543-013-0461-1) for numerically precise solve the problem. Now it only computes fixation probabilities for ranges of fitness values. Essentially, like [here](https://bitbucket.org/geodynapp/mom).

The folder `inputOfCppProgram` contains a [Sage](http://www.sagemath.org) program that computes automorphism groups of graphs to use with this software.

The software used for the paper is tagged `paper`.