//
//  main.cpp
//  phaseTrasitions
//
//  Created by Álvaro Lozano Rojo on 13/11/17.
//  Copyright © 2017 xx. All rights reserved.
//

#include <iostream>
#include <vector>
#include <tuple>
#include <bitset>
#include <map>
#include <list>
#include <string>
#include <fstream>
#include <time.h>
#include <stdlib.h>
#include <functional>

#include "rationals.hpp"
#include "bidiVector.hpp"
#include "solver.hpp"
#include "graphs_utils.hpp"
#include "ln22.hpp"
#include "friendshipstar.hpp"
#include "ln.hpp"

using namespace std;


// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}


string currentTime() {
    time_t rawtime;
    struct tm * timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    string ret(asctime(timeinfo));
    trim(ret);
    return "[" + ret + "]";
}



int main(int argc, const char * argv[]) {
    
    #pragma omp parallel for schedule(dynamic, 1)
    for(int order=6; order<=10; order+=2) {
        ln G(order);
        
        ofstream logfile(G.name("log"), ios::out | ios::trunc);

        logfile << currentTime() << "\tGraph of order " << G.size() << endl << endl << flush;
    
        // Construct reducer and reduced states
        logfile << currentTime() << "\tReducing " <<  (1<<G.size())-2 <<" non-trivial states to ... " << flush;
        G.computeStateReduction();
        const map<uint32_t, uint32_t>& stateReducer = G.getStateReducer();
        const map<uint32_t, size_t>& reducedStates = G.getReducedStates();

        logfile << reducedStates.size() << ". Finished at " << currentTime() <<flush << endl;
        
        logfile << currentTime() << "\tComputing symbolic transition matrix ... " << flush;

        // Transition matrices
        bidiVector<rational_t> Qr;
        bidiVector<rational_t> Q0;
        vector<rational_t> br;
        tie(Qr,Q0,br) = PQ(G, reducedStates, stateReducer);
        logfile << "Done!" << endl << flush;
    
        // We reduce the degree of the rational function. matSize is that
        // Expected degree
        int matSize = reducedStates.size();
    
        logfile << currentTime() << "\tWell, initial degree guess is " << matSize << ", like the of states."<< flush << endl;
    
        while(matSize > 0) {
            
            // The fitness values
            vector<rational_t> rs;
            rs.reserve(2*matSize);
            for(int i=1; i<=matSize+1; i++) rs.push_back(i);
            for(int i=2; i<=matSize; i++) {
                rational_t tmp = 1;
                tmp /= i;
                rs.push_back(tmp);
            }
            
            // The "base" matrices
            bidiVector<rational_t> V(2*matSize, 2*matSize);
            vector<rational_t> Vb(2*matSize);

            for(int i=0; i<Vb.size(); i++) {
                V(i, 0) = -1;
                V(i, 0+matSize) = 1;
                for(int j=1; j<matSize; j++) {
                    V(i, j+matSize) = rs[i] * V(i, j-1+matSize); // rs[i]**j
                    V(i, j) = -V(i,j+matSize);
                }
                Vb[i] = rs[i] * V(i, 2*matSize-1); // rs[i]**N
            }
            
            for(int i=0; i<rs.size(); i++) {
                auto r = rs[i];
                
                // Construct the matrices for a given r
                bidiVector<rational_t> Q(Q0);
                vector<rational_t> b(br);
                for(int j=0; j<Q.size(); j++) Q[j] += r * Qr[j];
                for(int j=0; j<b.size(); j++) b[j] *= r;
                
                solver(Q, b); // The solution is b
                
                // Since we made a reduction (by AG) we should make a
                // weighted mean
                
                // fixation probability
                rational_t Phi = 0;
                for(int j=0; j<G.size(); j++)
                    Phi += b[reducedStates.at( stateReducer.at(1<<j) )];
                Phi /= G.size();
                
                // To compute only for a single position
                // const uint8_t XX = 1;
                // Phi += b[reducedStates.at( stateReducer.at(1<<XX) )]; // Node XX

                
                Vb[i] *= (1-Phi);
                for(int j=matSize; j<V.nCols; j++)
                    V(i,j) *= Phi;
            }
            
            auto ret = solver(V, Vb);
            
            if(ret==-1) { // Solution found
                logfile << currentTime() <<"\tSolution found with degree " << matSize
                     << ". Saving to file '" << order << ".txt' and '" << order << ".sage' " << endl;
                
                ofstream txtfile(G.name("txt"), ios::out | ios::trunc);
                ofstream sagefile(G.name("sage"), ios::out | ios::trunc);

                for(int i=0; i<matSize; i++)
                    txtfile << Vb[i] << " ";
                txtfile << "1" << endl ;
                for(int i=0; i<matSize; i++)
                    txtfile << Vb[i+matSize] << " ";
                txtfile << "1" << endl ;

                sagefile << "(";
                if(Vb[0] != 0)
                    sagefile << Vb[0] << " + ";
                for(int i=1; i<matSize; i++)
                    if(Vb[i] != 0)
                        sagefile << Vb[i] << "*x^" << i << " + ";
                sagefile << "x^" << matSize << ")/(" ;
                if(Vb[0+matSize] != 0)
                    sagefile << Vb[0+matSize] << " + ";
                for(int i=1; i<matSize; i++)
                    if(Vb[i+matSize] != 0)
                        sagefile << Vb[i+matSize] << "*x^" << i << " + ";
                sagefile << "x^" << matSize << ")" << endl;

                txtfile.close();
                sagefile.close();
                
                /*for(int i=0; i<matSize; i++)
                    if(Vb[i] != 0)
                        cout << Vb[i] << "*r^" << i << " + ";
                cout << "r^" << matSize << endl;
                for(int i=0; i<matSize; i++)
                    cout << "-----------";
                cout << endl;
                for(int i=0; i<matSize; i++)
                    if(Vb[i+matSize] != 0)
                        cout << Vb[i+matSize] << "*r^" << i << " + ";
                cout << "r^" << matSize << endl;*/
                
                break;
            } else {
                // We reduce the expected order...
                // ret is were the pivot problem has been found...
                // So, in all those lines there is (at least) one "undefined" variable...
                // Hence, we have to reduce the degree in as many as pairs of variables (rounding upwards)
                int pairs = ceil((double)(2.0*matSize - ret) / 2.0);

                logfile << currentTime() << "\tReducing degree by " << pairs << endl << flush;
                matSize -= pairs;
            }
        }
        
    }
    return 0;
}
