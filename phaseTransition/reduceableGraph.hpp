//
//  reduceableGraph.hpp
//  phaseTransition
//
//  Created by Álvaro Lozano Rojo on 19/3/18.
//  Copyright © 2018 xx. All rights reserved.
//

#ifndef reduceableGraph_h
#define reduceableGraph_h

#include "graphs_utils.hpp"
#include <map>
#include <memory>
#include <string>

class reduceableGraph :  public graph_t {
public:
    virtual void computeStateReduction() const = 0;
    virtual const std::string name(const std::string& subfix = "") const = 0;

    const map<uint32_t, uint32_t>& getStateReducer() const {
        // Check if them have been computed
        if(not stateReducer) computeStateReduction();
        return *stateReducer;
    }
    
    const map<uint32_t, size_t>& getReducedStates() const {
        if(not reducedStates) computeStateReduction();
        return *reducedStates;
    }
    

protected:
    mutable std::unique_ptr<std::map<uint32_t, uint32_t>> stateReducer;
    mutable std::unique_ptr<std::map<uint32_t, size_t>> reducedStates;
};


#endif /* reduceableGraph_h */
