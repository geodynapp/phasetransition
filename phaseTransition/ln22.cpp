//
//  ln22.cpp
//  phaseTransitions_ln22
//
//  Created by Álvaro Lozano Rojo on 19/3/18.
//  Copyright © 2018 xx. All rights reserved.
//

#include "ln22.hpp"

using namespace std;

ln22::ln22(const uint8_t order) {
    // G is \ell_n^{2,2}. The first 6 nodes (0 to 5) form the bowtie
    // The rest is the large clique
    //
    //     0------1
    //    | \    / |
    //    |··3··4··|
    //    2········5
    //   =··········=
    //   =··K_n-2···=
    //    =········=
    //       =··=
    //
    // Fill the clique of L_n^{2,2}... starting from 2...
    for(uint8_t u = 2; u < order - 1; u++)
        for(uint8_t v = u + 1; v<order; v++)
            addEdge(u,v);
    
    // Now the bow tie... by hand
    addEdge(0, 1);
    addEdge(0, 2);
    addEdge(0, 3);
    addEdge(1, 4);
    addEdge(1, 5);
}

const string ln22::name(const string& subfix) const {
    return "l" + to_string(size()) + "_22" + ((subfix.empty())?"":("." + subfix));
}


void ln22::computeStateReduction() const {
    // Reset memory...
    stateReducer.reset(new map<uint32_t, uint32_t>());
    reducedStates.reset(new map<uint32_t, size_t>());

    for(uint32_t iconf=1; iconf < (1<<size())-1; iconf++){
        //
        // The state is lexycographically ordered... higher ones are the reduced
        // The state is
        //
        //  [0][1][2][3][4][5][...........]
        //
        // The rule are then:
        //   *  [....] should have all 1 at lower bits
        //   *  [0] >= [1]
        //   *  [2] >= [3]
        //   *  [4] >= [5]
        //   *  If [0] == [1] we ask [3,4] >= [4,5]
        uint32_t reduced = iconf;
        
        bool firstBitsSymetric = false;
        // Bits 0 and 1
        if( ((reduced & 0x1)?1:0) < ((reduced & 0x2)?1:0) ) {
            // First bits are 01-> but they must be 10->
            // We are changing node 0 with 1...
            // Hence we have to change the position of the bits 3,4 and 5,6
            // in the conf... for the rest of the argument
            reduced = ((reduced & 0xC) << 2) | ((reduced & 0x30) >> 2) | (reduced & 0xFFFFFFC0) | 0x1;
        } else {
            // Nothing to do
            firstBitsSymetric = not (static_cast<bool>(reduced & 0x1) xor static_cast<bool>(reduced & 0x2));
        }
        //Bits 2 and 3
        if( ((reduced & 0x4)?1:0) < ((reduced & 0x8)?1:0) ) {
            // We have XX01-> but we need XX10->
            reduced = (reduced & 0xFFFFFFF3) | 0x4;
        }
        
        //Bits 4 and 5
        if( ((reduced & 0x10)?1:0) < ((reduced & 0x20)?1:0) ) {
            // we have XXXX01-> but we need XXXX10->
            reduced = (reduced & 0xFFFFFFCF) | 0x10;
        }
        
        // If we have a simmetry in the first bits we can swap bit packets
        // 3-4 and 5-6 as wanted... bigger first
        if(firstBitsSymetric)
            if( ((reduced & 0xC) >>2) < ((reduced & 0x30) >> 4) ) {
                reduced = ((reduced & 0xC) << 2) | ((reduced & 0x30) >> 2) | (reduced & 0xFFFFFFC3);
            }
        
        // Rest of bits
        reduced &= 0x3F;
        for(int i=0; i<__builtin_popcount(iconf >> 6); i++)
            reduced |= (0x40 << i);
        
        // We have the reduced state, now...
        (*(this->stateReducer))[iconf] = reduced;
        if(this->reducedStates->find(reduced) == this->reducedStates->end()) { // New reduced word
            size_t last = this->reducedStates->size();
            (*(this->reducedStates))[reduced] = last;
        }
    }
}
