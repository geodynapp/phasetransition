//
//  graphFromIDandAutoMorphism.cpp
//  phaseTransitions_bp
//
//  Created by Álvaro Lozano Rojo on 27/3/18.
//  Copyright © 2018 xx. All rights reserved.
//

#include "graphFromIDandAutoMorphism.hpp"
#include<bitset>

using namespace std;

graphFromIDandAutoMorphism::graphFromIDandAutoMorphism(const uint64_t _Id, initializer_list<initializer_list<uint8_t>> _AG) : AG(), Id {_Id} {
    
    // Construct graph from Id
    uint64_t mask = 1;
    uint8_t u = 0;
    uint8_t v = 1;
    while(u < 10) {
        if(Id & mask) addEdge(u,v);
        mask <<= 1;
        v += 1;
        if(v > 10) {
            u += 1;
            v = u + 1;
        }
    }
            
    // Save group
    for(auto& g : _AG)
        AG.push_back(vector<uint8_t>(g));
}
    
void graphFromIDandAutoMorphism::computeStateReduction() const {
    // Initialize memory
    stateReducer.reset(new map<uint32_t, uint32_t>());
    reducedStates.reset(new map<uint32_t, size_t>());
    
    
    // Start with the reduction.
    for(uint32_t conf=1; conf < (1<<size())-1; conf++){
        // Check if it has been already added
        if( stateReducer->find(conf) != stateReducer->end() ) continue; // It has been added
        
        size_t last = reducedStates->size();
        (*reducedStates)[conf] = last;
        (*stateReducer)[conf] = conf; // The identity

        bitset<11> conf_bs(conf);
        for(auto& g : AG) {
            // Construct the orbit of the group action
            bitset<11> image_bs(0);
            for(uint8_t node = 0 ; node < size() ; node++)
                // Check if node is active. It it is, put it in the corresponding node
                image_bs[g[node]] = conf_bs[node];
            // Save result
            (*stateReducer)[image_bs.to_ulong()] = conf;
        }
    }

}

const string graphFromIDandAutoMorphism::name(const string& subfix) const {
    return to_string(Id) + ((subfix.empty())?"":("." + subfix));
}
    

