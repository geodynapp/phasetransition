//
//  main.cpp
//  phaseTrasitions
//
//  Created by Álvaro Lozano Rojo on 13/11/17.
//  Copyright © 2017 xx. All rights reserved.
//

#include <iostream>
#include <vector>
#include <tuple>
#include <bitset>
#include <map>
#include <list>
#include <string>
#include <fstream>
#include <time.h>
#include <stdlib.h>

#include "rationals.hpp"
#include "bidiVector.hpp"
#include "solver.hpp"
#include "graphs_utils.hpp"

using namespace std;


// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}


string currentTime() {
    time_t rawtime;
    struct tm * timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    string ret(asctime(timeinfo));
    trim(ret);
    return "[" + ret + "]";
}



int main(int argc, const char * argv[]) {
    
    if(argc != 2){
        cout << "Usage: " << argv[0] << " graphsFile" << endl
             << "graphsFile contains lines of the form ID AutomorphismGroup" << endl
             << "The automorphism group should be of the form #,...,#|#,...,#|...|#,...,#" << endl;
        return 0;
    }
    
    cout << currentTime() << "\t" << "Reading file and building graps..." << flush << endl;
    
    ifstream infile(argv[1] , ios::in);
    string line;
    
    vector<tuple<uint64_t, graph_t, ag_t>*> graphs;
    
    while(std::getline(infile, line)) {
        vector<string> tokens;

        // Divide en dos
        size_t pos;
        if((pos = line.find(" ")) != string::npos) {
            tokens.push_back(line.substr (0, pos));
            line.erase(0, pos + 1);
        }
        if(line.size()>0)
            tokens.push_back(line);
        
        tuple<uint64_t, graph_t, ag_t>* tmp = new tuple<uint64_t, graph_t, ag_t>;
        graphs.push_back(tmp);
        
        char *ptr;
        uint64_t GID = strtoull(tokens[0].c_str(), &ptr, 10);
        get<0>(*tmp) = GID;
        get<1>(*tmp) = id2el(GID);
        ag_t& AG = get<2>(*tmp);

        if(tokens.size()==2) {
            string& s = tokens[1];
            list<string> elements;
            
            // Divide en los generadores
            size_t pos = 0;
            while ((pos = s.find ("|")) != string::npos) {
                elements.push_back(s.substr (0, pos));
                s.erase (0, pos + 1);
            }
            elements.push_back(s);
            
            // Genera los vectores de elementos
            for(auto& gs : elements) {
                vector<uint8_t> g;
                g.reserve(get<1>(*tmp).size());
                pos = 0;
                while ((pos = gs.find (",")) != string::npos) {
                    g.push_back(atoi(gs.substr (0, pos).c_str()));
                    gs.erase (0, pos + 1);
                }
                g.push_back(atoi(gs.c_str()));
                
                if(g.size() != get<1>(*tmp).size()) {
                    // El elemento leido no es del tamaño correcto...
                    cout << "ERROR! El grafo " << GID << " tiene por elemento del grupo de automorfismos a ( ";
                    for(auto v : g) cout << v << " ";
                    cout << " )... que no es de la buena longitud (" << get<1>(*tmp).size() <<")"<<endl;
                    return -1;
                }
                
                AG.push_back(g);
            }
        }
    }
    
    cout << currentTime() << "\t" << "Done" << flush << endl;

    infile.close();
    
    #pragma omp parallel for schedule(dynamic, 1)
    for(int gpos=0; gpos<graphs.size(); gpos++) {
        uint64_t GID = get<0>(*graphs[gpos]);
        graph_t &G = get<1>(*graphs[gpos]);
        ag_t &AG = get<2>(*graphs[gpos]);
        
        ofstream logfile(to_string(GID) + ".log" , ios::out | ios::trunc);

    
        logfile << currentTime() << "\tGraph Id=" << GID << " of order " << G.size() << endl << endl;
    
        if(AG.size()>0) {
            logfile << "\tAutomorphism Group elements:" << endl;
            for(auto& g : AG) {
                logfile << "\t\t( ";
                for(auto val : g)
                    logfile << (int)val << " ";
                logfile << ")" << endl;
            }
        } else {
            logfile << "\tTrivial automorphism group" << endl;
        }
        logfile << endl << flush;
        
        map<uint32_t, uint32_t> stateReducer;
        map<uint32_t, size_t> reducedStates;
    
        logfile << currentTime() << "\tReducing " <<  (1<<G.size())-2 <<" states to ... " << flush;
        for(uint32_t conf=1; conf < (1<<G.size())-1; conf++){
            if(stateReducer.find(conf) == stateReducer.end()) { // Configuración todavía no usada
                size_t last = reducedStates.size();
                reducedStates[conf] = last;
                stateReducer[conf] = conf;
                for(auto& g : AG)
                    stateReducer[ reduce(conf, g, G.size()) ] = conf;
            }
        }
        logfile << reducedStates.size() << ". Finished at " << currentTime() <<flush << endl;
    
        logfile << currentTime() << "\tComputing symbolic transition matrix ... " << flush;
        // Transition matrices
        bidiVector<rational_t> Qr;
        bidiVector<rational_t> Q0;
        vector<rational_t> br;
        tie(Qr,Q0,br) = PQ(G, reducedStates, stateReducer);
        logfile << "Done!" << endl << flush;
    
        // We reduce the degree of the rational function. matSize is that
        // Expected degree
        int matSize = reducedStates.size();
    
        logfile << currentTime() << "\tWell, initial degree guess is " << matSize << ", like the of states."<< flush << endl;
    
        while(matSize > 0) {
            
            // Fitness values
            vector<rational_t> rs;
            rs.reserve(2*matSize);
            for(int i=1; i<=matSize+1; i++) rs.push_back(i);
            for(int i=2; i<=matSize; i++) {
                rational_t tmp = 1;
                tmp /= i;
                rs.push_back(tmp);
            }
            
            // The "base" matrices
            bidiVector<rational_t> V(2*matSize, 2*matSize);
            vector<rational_t> Vb(2*matSize);

            for(int i=0; i<Vb.size(); i++) {
                V(i, 0) = -1;
                V(i, 0+matSize) = 1;
                for(int j=1; j<matSize; j++) {
                    V(i, j+matSize) = rs[i] * V(i, j-1+matSize); // rs[i]**j
                    V(i, j) = -V(i,j+matSize);
                }
                Vb[i] = rs[i] * V(i, 2*matSize-1); // rs[i]**N
            }
            
            for(int i=0; i<rs.size(); i++) {
                auto r = rs[i];
                
                // Construct the matrices for a given r
                bidiVector<rational_t> Q(Q0);
                vector<rational_t> b(br);
                for(int j=0; j<Q.size(); j++) Q[j] += r * Qr[j];
                for(int j=0; j<b.size(); j++) b[j] *= r;
                
                solver(Q, b); // The solution is b
                
                // Since we made a reduction (by AG) we should make a
                // weighted mean
                
                // fixation probability
                rational_t Phi = 0;
                for(int j=0; j<G.size(); j++)
                    Phi += b[reducedStates[stateReducer[(1<<j)]]];
                Phi /= G.size();
                
                Vb[i] *= (1-Phi);
                for(int j=matSize; j<V.nCols; j++)
                    V(i,j) *= Phi;
            }
            
            auto ret = solver(V, Vb);
            
            if(ret==-1) { // Solution found
                logfile << currentTime() <<"\tSolution found with degree " << matSize
                     << ". Saving to file '" << GID << ".txt' and '" << GID << ".sage' " << endl;
                
                ofstream txtfile(to_string(GID) + ".txt" , ios::out | ios::trunc);
                ofstream sagefile(to_string(GID) + ".sage" , ios::out | ios::trunc);

                for(int i=0; i<matSize; i++)
                    txtfile << Vb[i] << " ";
                txtfile << "1" << endl ;
                for(int i=0; i<matSize; i++)
                    txtfile << Vb[i+matSize] << " ";
                txtfile << "1" << endl ;

                sagefile << "(";
                if(Vb[0] != 0)
                    sagefile << Vb[0] << " + ";
                for(int i=1; i<matSize; i++)
                    if(Vb[i] != 0)
                        sagefile << Vb[i] << "*x^" << i << " + ";
                sagefile << "x^" << matSize << ")/(" ;
                if(Vb[0+matSize] != 0)
                    sagefile << Vb[0+matSize] << " + ";
                for(int i=1; i<matSize; i++)
                    if(Vb[i+matSize] != 0)
                        sagefile << Vb[i+matSize] << "*x^" << i << " + ";
                sagefile << "x^" << matSize << ")" << endl;

                txtfile.close();
                sagefile.close();
                
                /*for(int i=0; i<matSize; i++)
                    if(Vb[i] != 0)
                        cout << Vb[i] << "*r^" << i << " + ";
                cout << "r^" << matSize << endl;
                for(int i=0; i<matSize; i++)
                    cout << "-----------";
                cout << endl;
                for(int i=0; i<matSize; i++)
                    if(Vb[i+matSize] != 0)
                        cout << Vb[i+matSize] << "*r^" << i << " + ";
                cout << "r^" << matSize << endl;*/
                
                break;
            } else {
                // We reduce the expected order...
                // ret is were the pivot problem has been found...
                // So, in all those lines there is (at least) one "undefined" variable...
                // Hence, we have to reduce the degree in as many as pairs of variables (rounding upwards)
                int pairs = ceil((double)(2.0*matSize - ret) / 2.0);

                logfile << currentTime() << "\tReducing degree by " << pairs << endl << flush;
                matSize -= pairs;
            }
        }
        
        delete graphs[gpos];
    }
    return 0;
}
