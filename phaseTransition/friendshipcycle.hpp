//
//  friendshipcycle.hpp
//  phaseTransitions_ln22
//
//  Created by Álvaro Lozano Rojo on 26/3/18.
//  Copyright © 2018 xx. All rights reserved.
//

#ifndef friendshipcycle_hpp
#define friendshipcycle_hpp

#include<functional>
#include<vector>
#include<string>
#include "reduceableGraph.hpp"

class friendshipcycle : public reduceableGraph {
public:
    friendshipcycle() = delete;
    friendshipcycle(const int N);
    
    void computeStateReduction() const;
    const std::string name(const std::string& subfix = "") const;

private:
    std::vector<std::function<uint8_t(uint8_t)>> Gsym;
};

#endif /* friendshipcycle_hpp */
