#ifndef _BIDIVECTOR__HPP
#define _BIDIVECTOR__HPP


#include <iostream>
#include <vector>

//
// A simple bidimensional array class
//
template<class T> class bidiVector : public std::vector<T> {
public:
    size_t nRows;
    size_t nCols;
    
    // Contructores
    bidiVector () : std::vector<T> (0) {
        nRows = nCols = 0;
    }
    bidiVector (const bidiVector<T>& other) : std::vector<T>(0) {
        operator=(other);
    }
    bidiVector (const size_t _nRows, const size_t _nCols) : std::vector<T> (_nRows * _nCols) {
        nRows = _nRows;
        nCols = _nCols;
    }
    bidiVector (const size_t _nRows, const size_t _nCols, const T& val) : std::vector<T> (_nRows * _nCols, val) {
        nRows = _nRows;
        nCols = _nCols;
    }

    // Los métodos que usaremos
    inline void resize (const size_t _nRows, const size_t _nCols, const T& val) {
        std::vector<T>::resize (_nRows * _nCols, val);
        nRows = _nRows;
        nCols = _nCols;
    }
    
    inline void resize (const size_t _nRows, const size_t _nCols) {
        std::vector<T>::resize (_nRows * _nCols);
        nRows = _nRows;
        nCols = _nCols;
    }
    
    T& operator() (int row, int col){
        auto i = std::vector<T>::begin ();
        return *(i + (row * nCols + col));
    }
    
    const T& operator() (const int row, const int col) const {
        return std::vector<T>::operator[](row * nCols + col);
    }
    
    bidiVector<T>& operator= (const bidiVector<T>& other) {
        if(this != &other) {
            nRows = other.nRows;
            nCols = other.nCols;
            std::vector<T>::operator=(other);
        }
        return *this;
    }

    void pretty_print() const {
        for(int i=0; i<nRows; i++) {
            for(int j=0; j<nCols; j++)
                std::cout << (*this)(i,j) << "\t";
            std::cout << std::endl;
        }
    }
    
};


#endif /* bideVector_h */

