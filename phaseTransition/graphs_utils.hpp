//
//  graphs_utils.hpp
//  phaseTrasitions
//
//  Created by Álvaro Lozano Rojo on 15/11/17.
//  Copyright © 2017 xx. All rights reserved.
//

#ifndef graphs_utils_hpp
#define graphs_utils_hpp

#include "rationals.hpp"
#include "bidiVector.hpp"
#include <tuple>
#include <vector>
#include <functional>
#include <list>
#include <map>

class graph_t : public std::map<uint8_t, std::list<uint8_t>> {
public:
    void addEdge(const uint8_t u, const uint8_t v);
    
    void printMe() const;
};


typedef std::list<std::vector<uint8_t>> ag_t;

graph_t id2el(const uint64_t Id);
std::tuple<bidiVector<rational_t>,bidiVector<rational_t>,std::vector<rational_t>> PQ(const graph_t& G);
std::tuple<bidiVector<rational_t>,bidiVector<rational_t>,std::vector<rational_t>> PQ(const graph_t& G, const std::map<uint32_t, size_t>& reducedStates, const std::map<uint32_t, uint32_t>& stateReducer);
uint32_t reduce(const uint32_t conf, std::vector<uint8_t>& g, const size_t order);

#endif /* graphs_utils_hpp */
