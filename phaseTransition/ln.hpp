//
//  ln.hpp
//  phaseTransitions_ln22
//
//  Created by Álvaro Lozano Rojo on 15/6/18.
//  Copyright © 2018 xx. All rights reserved.
//

#ifndef ln_hpp
#define ln_hpp


#include "graphs_utils.hpp"
#include <map>
#include <memory>
#include <string>
#include "reduceableGraph.hpp"

class ln : public reduceableGraph {
public:
    ln() = delete;
    ln(const uint8_t order);
    
    void computeStateReduction() const;
    const std::string name(const std::string& subfix = "") const;
};



#endif /* ln_hpp */
