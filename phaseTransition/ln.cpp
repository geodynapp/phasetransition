//
//  ln.cpp
//  phaseTransitions_ln22
//
//  Created by Álvaro Lozano Rojo on 15/6/18.
//  Copyright © 2018 xx. All rights reserved.
//

#include "ln.hpp"
#include <bitset>

using namespace std;

ln::ln(const uint8_t order) {
    // G is \ell_n.
    //
    //     0------n-1
    //    | \    / |
    //    |········|
    //    ··········
    //   =··········=
    //   =··K_n-2···=
    //    =········=
    //       =··=
    //
    // Fill the clique of L_n... starting from 1 to order -1...
    for(uint8_t u = 1; u < order - 2; u++)
        for(uint8_t v = u + 1; v<order-1; v++)
            addEdge(u,v);
    
    // Now the bow tie... by hand
    for(uint8_t i = 1; i < order/2; i++) {
        addEdge(0, i);
        addEdge(order-1, order/2 - 1 + i);
    }

    // Connection between new ones
    addEdge(0, order-1);
}

const string ln::name(const string& subfix) const {
    return "l" + to_string(size()) + ((subfix.empty())?"":("." + subfix));
}


void ln::computeStateReduction() const {
    // Reset memory...
    stateReducer.reset(new map<uint32_t, uint32_t>());
    reducedStates.reset(new map<uint32_t, size_t>());
    
    for(uint32_t iconf=1; iconf < (1<<size())-1; iconf++){
        
        const bitset<32> conf(iconf);
        bitset<32> reduced(0);
        
        bool swapped = false;
        if(conf[0]) { // The extreme points
            reduced[0] = 1;
            reduced[size()-1] = conf[size()-1];
        } else if(conf[size()-1]) {
            reduced[0] = 1;
            reduced[size()-1] = 0;
            swapped = true;
        }
        
        // now the two halves
        int left = 0;
        int right = 0;
        for(int i=1; i<size()/2; i++) {
            if(conf[i]) left++;
            if(conf[size()/2 - 1 + i]) right++;
        }
            
        if(swapped) {
            auto x = left;
            left = right;
            right = x;
        }
        
        if(reduced[0] == reduced[size()-1]) { // I can swap if I whant the
            if(left < right) {
                auto x = left;
                left = right;
                right = x;
            }
        }
        
        // Ok... put the number in the reduced thing
        for(int i=1; i<=left; i++)
            reduced[i] = 1;
        for(int i=0; i<right; i++)
            reduced[size()/2 + i] = 1;
        
        // We have the reduced state, now...
        (*(this->stateReducer))[iconf] = reduced.to_ulong();
        if(this->reducedStates->find(reduced.to_ulong()) == this->reducedStates->end()) { // New reduced word
            size_t last = this->reducedStates->size();
            (*(this->reducedStates))[reduced.to_ulong()] = last;
        }
    }
}
