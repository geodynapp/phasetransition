#ifndef _BPSOLVER_H
#define _BPSOLVER_H

typedef double real_t;

void bpsolver( int n, real_t *A , real_t *b , real_t *r, real_t *x );

#endif
