//
//  solver.hpp
//  phaseTrasitions
//
//  Created by Álvaro Lozano Rojo on 13/11/17.
//  Copyright © 2017 xx. All rights reserved.
//

#ifndef solver_hpp
#define solver_hpp

#include "rationals.hpp"
#include "bidiVector.hpp"
#include <map>
#include <vector>

int solver(bidiVector<rational_t>& Q, std::vector<rational_t>& b);


#endif /* solver_hpp */
