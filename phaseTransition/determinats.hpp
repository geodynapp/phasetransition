//
//  determinats.hpp
//  phaseTransition
//
//  Created by Álvaro Lozano Rojo on 7/3/18.
//  Copyright © 2018 xx. All rights reserved.
//

#include <utility>

#ifndef determinats_h
#define determinats_h

typedef std::pair<size_t,size_t> coordinate;
bool operator<(const coordinate &a, const coordinate &b) {
    if(a.first < b.first) return true;
    if(a.first == b.first)
        if(a.second < b.second) return true;
    return false;
}



template<class T> class abstractSqMat;
template<class T, size_t N> class sqMat;
template<class T> class sqSubMat;


template<class T>
class abstractSqMat {
public:
    virtual const T& operator() (size_t i, size_t j) const = 0;
    virtual T& operator() (size_t i, size_t j) = 0;
    virtual size_t size() const  = 0;
    T det() {
        size_t N = size();
        if(N == 1) return (*this)(0,0);
        
        T accumulator {0};
        bool sign = false;
        for(int row=0; row < N; row++) {
            T val = (*this)(row,0);
            if(val == 0) continue;
            if(sign)
                accumulator -= (*this)(row,0) * subMat(row,0).det();
            else
                accumulator += (*this)(row,0) * subMat(row,0).det();
            sign = not sign;
        }
        
        return accumulator;
    }
    sqSubMat<T> subMat(size_t row, size_t col) {
        return sqSubMat<T>(row, col, *this);
    }
    
    friend std::ostream& operator<<(std::ostream& os, const abstractSqMat<T>& A) {
        size_t N = A.size();
        if(N!=0)
            for(int j=0; j<N; j++) {
                os << "[ ";
                for(int i=0; i<N; i++)
                    os << A(i,j) << " ";
                os << "]" << std::endl;
            }
        else
            os << "[]" << std::endl;
        
        return os;
    }
};


template<class T>
class sqSubMat : public abstractSqMat<T> {
public:
    sqSubMat() = delete;
    sqSubMat(size_t rows_, size_t cols_, abstractSqMat<T>& superMat) : super {superMat}, rows{rows_}, cols{cols_}, N{superMat.size() - 1} {}
    
    T& operator() (size_t i, size_t j) {
        if(i >= rows) i++;
        if(j >= cols) j++;
        return super(i,j);
    }
    
    const T& operator() (size_t i, size_t j) const {
        if(i >= rows) i++;
        if(j >= cols) j++;
        return super(i,j);
    }
    
    size_t size() const {
        return N;
    }
    
protected:
    abstractSqMat<T>& super;
    size_t rows;
    size_t cols;
    size_t N;
};

template<class T, size_t N>
class sqMat : public abstractSqMat<T> {
public:
    sqMat() {}
    
    const T& operator() (const size_t i, const size_t j) const {
        return data[N*i + j];
    }

    T& operator() (const size_t i, const size_t j) {
        return data[N*i + j];
    }
    
    size_t size() const {
        return N;
    }
    
protected:
    T data[N*N];
};



#endif /* determinats_h */
