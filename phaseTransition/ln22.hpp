//
//  ln22.hpp
//  phaseTransitions_ln22
//
//  Created by Álvaro Lozano Rojo on 19/3/18.
//  Copyright © 2018 xx. All rights reserved.
//

#ifndef ln22_hpp
#define ln22_hpp

#include "graphs_utils.hpp"
#include <map>
#include <memory>
#include <string>
#include "reduceableGraph.hpp"

class ln22 : public reduceableGraph {
public:
    ln22() = delete;
    ln22(const uint8_t order);
    
    void computeStateReduction() const;
    const std::string name(const std::string& subfix = "") const;
};


#endif /* ln22_hpp */
