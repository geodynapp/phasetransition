//
//  frienshipstar.cpp
//  phaseTransitions_ln22
//
//  Created by Álvaro Lozano Rojo on 19/3/18.
//  Copyright © 2018 xx. All rights reserved.
//

#include "friendshipstar.hpp"

using namespace std;

friendshipstar::friendshipstar(const uint8_t petals) {
    for(int i=0; i<petals; i++) {
        addEdge(0, 2*i+1);
        addEdge(0, 2*i+2);
        addEdge(2*i+1, 2*i+2);
    }
}

const string friendshipstar::name(const string& subfix) const {
    return "frienshipstar" + to_string((size()-1)/2) + ((subfix.empty())?"":("." + subfix));
}

void friendshipstar::computeStateReduction() const {
    // The reduction is quite simple... just count the number number of full, half and empty petals
    // And order in that way!
    
    this->stateReducer.reset(new map<uint32_t, uint32_t>());
    reducedStates.reset(new map<uint32_t, size_t>());
    const int petals = (size()-1)/2;
    
    for(uint32_t conf=1; conf < (1<<size())-1; conf++){
        int full = 0;
        int half = 0;
        int empty = 0;
    
        uint32_t mask = 0x6; // second and third bits set...
        int move = 1;
        for(int i=0; i<petals; i++) {
            uint32_t petalConf = (conf & mask) >> move;
        
            if(petalConf == 0) empty++;
            else if(petalConf == 0x3) full++;
            else half++;
        
            mask <<= 2;
            move +=2;
        }
        
        // Ok... now we know the numbers of petal classes and we can construct the thing
        // Save the first bit... that is the status of 0
        uint32_t reduced = (conf & 0x1);
        
        // Add the bits corresponding to full petals.. that is 2^(2*full)-1 (moved a bit), i.e., 2^(2*full+1)-2
        reduced |= (1 << (2*full+1)) - 2;
        
        // Add the bits of the half filled... that is 10101010...
        // a sum of the form 2 + 2^3 + 2^5 ··· + 2^(2*half-1) = ( 2^(2*half) - 2 )/3
        // Moved 1 for the first bit and 2*full for the full petals
        reduced |= (((2 << (2*half)) - 2)/3) << (1+2*full);
        
        // We have the reduced state, now...
        (*(this->stateReducer))[conf] = reduced;
        if(this->reducedStates->find(reduced) == this->reducedStates->end()) { // New reduced word
            size_t last = this->reducedStates->size();
            (*(this->reducedStates))[reduced] = last;
        }
    }
}




