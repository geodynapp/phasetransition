//
//  rationals.hpp
//  phaseTrasitions
//
//  Created by Álvaro Lozano Rojo on 13/11/17.
//  Copyright © 2017 xx. All rights reserved.
//

#ifndef rationals_h
#define rationals_h


#include <boost/multiprecision/gmp.hpp>
#include <boost/multiprecision/cpp_int.hpp>

typedef boost::multiprecision::mpq_rational rational_t;
//typedef double rational_t;


#endif /* rationals_h */
