//
//  main.cpp
//  phaseTrasitions
//
//  Created by Álvaro Lozano Rojo on 13/11/17.
//  Copyright © 2017 xx. All rights reserved.
//

#include <iostream>
#include <vector>
#include <tuple>
#include <bitset>
#include <map>
#include <list>
#include <string>
#include <fstream>
#include <time.h>
#include <stdlib.h>
#include <functional>
#include <limits>

#include "rationals.hpp"
#include "bidiVector.hpp"
#include "solver.hpp"
#include "graphs_utils.hpp"
#include "ln22.hpp"
#include "friendshipstar.hpp"
#include "friendshipcycle.hpp"
#include "graphFromIDandAutoMorphism.hpp"
#include "bpsolver.h"

using namespace std;


// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}


string currentTime() {
    time_t rawtime;
    struct tm * timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    string ret(asctime(timeinfo));
    trim(ret);
    return "[" + ret + "]";
}



int main(int argc, const char * argv[]) {
    
#pragma omp parallel for schedule(dynamic, 1)
    //for(int order=6; order<=15; order++) {
    {
        //friendshipcycle G(order);
        graphFromIDandAutoMorphism G(1306380925976600ULL, {{0,1,3,2,4,5,6,7,8,9}, {0,1,2,3,5,4,6,7,8,9}, {0,1,2,3,4,5,7,6,8,9}, {1,0,2,3,6,7,4,5,9,8}, {0,1,3,2,4,5,7,6,8,9}, {1,0,2,3,6,7,5,4,9,8}, {0,1,2,3,5,4,7,6,8,9}, {1,0,3,2,6,7,4,5,9,8}, {0,1,3,2,5,4,6,7,8,9}, {1,0,2,3,7,6,4,5,9,8}, {1,0,3,2,7,6,4,5,9,8}, {1,0,2,3,7,6,5,4,9,8}, {1,0,3,2,6,7,5,4,9,8}, {0,1,3,2,5,4,7,6,8,9}, {1,0,3,2,7,6,5,4,9,8}});
        
        ofstream logfile(G.name("log"), ios::out | ios::trunc);
        
        logfile << currentTime() << "\tGraph of order " << G.size() << endl << endl << flush;
        
        // Construct reducer and reduced states
        logfile << currentTime() << "\tReducing " <<  (1<<G.size())-2 <<" non-trivial states to ... " << flush;
        G.computeStateReduction();
        const map<uint32_t, uint32_t>& stateReducer = G.getStateReducer();
        const map<uint32_t, size_t>& reducedStates = G.getReducedStates();
        
        logfile << reducedStates.size() << ". Finished at " << currentTime() <<flush << endl;
        
        logfile << currentTime() << "\tComputing symbolic transition matrix ... " << flush;
        
        // Transition matrices
        bidiVector<rational_t> Qr;
        bidiVector<rational_t> Q0;
        vector<rational_t> br;
        tie(Qr,Q0,br) = PQ(G, reducedStates, stateReducer);
        logfile << "Done! " << currentTime() << endl << flush;
        
        rational_t frac110 = 1;
        frac110 /= 10;
        rational_t frac14 = 1;
        frac14 /= 4;
        
        
        ofstream txtfile(G.name("txt") , ios::out | ios::trunc);
        txtfile << setprecision(numeric_limits<double>::max_digits10);
        
        for(rational_t r=9.75; r <= 120; r += (r<4)?frac110:frac14) {
            // Eval matrix for the current r
            const size_t TAM = Qr.nCols;
            
            // Reserve clean memory
            bidiVector<real_t> Q(TAM, TAM, 0.0);
            vector<real_t> b(TAM, 0.0);
            vector<real_t> Qsum(TAM, 0.0);
            vector<real_t> X(TAM, 0.0);
            
            // The negative sign is because how PQ acts...
            auto itQ = Q.begin();
            auto itQr = Qr.begin();
            auto itQ0 = Q0.begin();
            // Compute Q for this value of r
            while(itQ != Q.end()) {
                *(itQ++) = -static_cast<double>(*itQr++ * r + *itQ0++);
            }
            
            // Compute b for this value of r
            auto itbr = br.begin();
            auto itb = b.begin();
            while(itb != b.end()) {
                *(itb++) = -static_cast<double>(*itbr++ * r);
            }
            
            // Compute the sum of the row of Q.
            // I'm lazy, not like in mom... just directly compute the sum
            for(int row=0; row<TAM; row++) {
                rational_t tmpr, tmp0;
                for(int col=0; col<TAM; col++) {
                    tmpr += Qr(row,col);
                    tmp0 += Q0(row,col);
                }
                Qsum[row] = -static_cast<double>(tmpr * r + tmp0);
            }
            
            // Solve the system
            bpsolver(TAM, Q.data(), b.data(), Qsum.data(), X.data());
            
            
            // Compute mean fixation probability
            real_t Phi = 0;
            for(int j=0; j<G.size(); j++)
                Phi += X[reducedStates.at( stateReducer.at(1<<j) )];
            Phi /= G.size();

            txtfile << static_cast<double>(r) << "\t" << Phi << endl << flush;
        }
    }
    return 0;
}

