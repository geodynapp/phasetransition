//
//  graphs_utils.cpp
//  phaseTrasitions
//
//  Created by Álvaro Lozano Rojo on 15/11/17.
//  Copyright © 2017 xx. All rights reserved.
//

#include "graphs_utils.hpp"

using namespace std;

uint32_t reduce(const uint32_t conf, vector<uint8_t>& g, const size_t order) {
    uint32_t res = 0;
    for(int bit=0; bit<order; bit++)
        if(conf & (1<<bit))
            res |= (1 << g[bit]);
    return res;
}

graph_t id2el(const uint64_t Id) {
    graph_t el;
    uint64_t mask = 1;
    uint8_t u = 0;
    uint8_t v = 1;
    while(u < 10) {
        if(Id & mask) el.addEdge(u,v);
        
        mask <<= 1;
        v += 1;
        if(v > 10) {
            u += 1;
            v = u + 1;
        }
    }
    return el;
}


tuple<bidiVector<rational_t>,bidiVector<rational_t>,vector<rational_t>> PQ(const graph_t& G){
    
    size_t N = G.size();
    
    uint64_t matSize = (1<<N)-2;
    
    bidiVector<rational_t> Qr(matSize, matSize);
    bidiVector<rational_t> Q0(matSize, matSize);
    vector<rational_t> br(matSize);
    
    const uint32_t totalState = (1<<N) - 1;
    
    for(uint32_t conf=1; conf<totalState; conf++) {
        // Peso reproductivo total TRW = (Muta*r + N - Muta)
        // Empezamos con -Id * TRW
        int Muta = __builtin_popcount(conf);
        int confPos = conf - 1;
        
        Qr(confPos, confPos) = -Muta;
        Q0(confPos, confPos) = Muta-((int)N);
        
        for(auto& it : G) {
            uint8_t u = it.first;
            
            uint32_t uMutante = conf & (1 << u);
            
            //u se ha seleccionado para reproducirse
            for(auto v : it.second /* G[u] */) {
                //v será reemplazado
                if(uMutante){ //u es mutante
                    //tenemos que coger conf y añadirle un bit en 1 en la posicion v
                    uint32_t dest = conf | (1 << v);
                    if(dest != totalState) {
                        rational_t tmp = 1;
                        tmp /= (int)(G.at(u).size());
                        Qr(confPos, dest-1) += tmp;
                    } else {
                        rational_t tmp = 1;
                        tmp /= (int)(G.at(u).size());
                        br[confPos] -= tmp;
                    }
                } else {
                    //Tengo que coger conf y poner un 0 en la posicion v...
                    uint32_t dest = conf & (~(1 << v));
                    if(dest != 0) {
                        rational_t tmp = 1;
                        tmp /= (int)(G.at(u).size());
                        Q0(confPos, dest-1) += tmp;
                    }
                }
            }
        }
    }
    
    return make_tuple(Qr,Q0,br);
}


tuple<bidiVector<rational_t>,bidiVector<rational_t>,vector<rational_t>> PQ(const graph_t& G, const map<uint32_t, size_t>& reducedStates, const map<uint32_t, uint32_t>& stateReducer){
    
    size_t N = G.size();
    
    uint64_t matSize = reducedStates.size();
    
    bidiVector<rational_t> Qr(matSize, matSize);
    bidiVector<rational_t> Q0(matSize, matSize);
    vector<rational_t> br(matSize);
    
    const uint32_t totalState = (1<<N) - 1;
    
    for(auto& el : reducedStates) {
        auto conf = el.first;
        auto confPos = el.second;
        
        // Peso reproductivo total TRW = (Muta*r + N - Muta)
        // Empezamos con -Id * TRW
        int Muta = __builtin_popcount(conf);
        
        Qr(confPos, confPos) = -Muta;
        Q0(confPos, confPos) = Muta-((int)N);
        
        for(auto& it : G) {
            uint8_t u = it.first;
            
            uint32_t uMutante = conf & (1 << u);
            
            //u se ha seleccionado para reproducirse
            for(auto v : it.second /* G[u] */) {
                //v será reemplazado
                if(uMutante){ //u es mutante
                    //tenemos que coger conf y añadirle un bit en 1 en la posicion v
                    uint32_t dest = (conf | (1 << v));
                    if(dest != totalState) {
                        dest = stateReducer.at(dest);
                        rational_t tmp = 1;
                        tmp /= (int)(G.at(u).size());
                        Qr(confPos, reducedStates.at(dest)) += tmp;
                    } else {
                        rational_t tmp = 1;
                        tmp /= (int)(G.at(u).size());
                        br[confPos] -= tmp;
                    }
                } else {
                    //Tengo que coger conf y poner un 0 en la posicion v...
                    uint32_t dest = (conf & (~(1 << v)));
                    if(dest != 0) {
                        dest = stateReducer.at(dest);
                        rational_t tmp = 1;
                        tmp /= (int)(G.at(u).size());
                        Q0(confPos, reducedStates.at(dest)) += tmp;
                    }
                }
            }
        }
    }
    
    return make_tuple(Qr,Q0,br);
}


void graph_t::addEdge(const uint8_t u, const uint8_t v) {
    (*this)[u].push_front(v);
    (*this)[v].push_front(u);
}

void graph_t::printMe() const {
    for(auto &el : *this) {
        cout << (int)el.first << " [ ";
        for(auto v: el.second)
            cout << (int)v << " ";
        cout << "]"<< endl;
    }
}


