//
//  frienshipstar.hpp
//  phaseTransitions_ln22
//
//  Created by Álvaro Lozano Rojo on 19/3/18.
//  Copyright © 2018 xx. All rights reserved.
//

#ifndef frienshipstar_hpp
#define frienshipstar_hpp


#include "reduceableGraph.hpp"
#include <map>
#include <memory>
#include <string>

class friendshipstar : public reduceableGraph {
public:
    friendshipstar() = delete;
    friendshipstar(const uint8_t petals);
    
    void computeStateReduction() const;
    const std::string name(const std::string& subfix = "") const;
};


#endif /* frienshipstar_hpp */
