//
//  graphFromIDandAutoMorphism.hpp
//  phaseTransitions_bp
//
//  Created by Álvaro Lozano Rojo on 27/3/18.
//  Copyright © 2018 xx. All rights reserved.
//

#include "reduceableGraph.hpp"
#include <map>
#include <memory>
#include <string>
#include <initializer_list>

#ifndef graphFromIDandAutoMorphism_hpp
#define graphFromIDandAutoMorphism_hpp

class graphFromIDandAutoMorphism : public reduceableGraph {
public:
    graphFromIDandAutoMorphism() = delete;
    graphFromIDandAutoMorphism(const uint64_t ID, std::initializer_list<std::initializer_list<uint8_t>> _AG);
    
    void computeStateReduction() const;
    const std::string name(const std::string& subfix = "") const;
    
private:
    std::vector<std::vector<uint8_t>> AG;
    uint64_t Id;
};



#endif /* graphFromIDandAutoMorphism_hpp */
