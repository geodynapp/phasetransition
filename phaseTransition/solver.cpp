//
//  solver.cpp
//  phaseTrasitions
//
//  Created by Álvaro Lozano Rojo on 13/11/17.
//  Copyright © 2017 xx. All rights reserved.
//

#include "solver.hpp"
#include <memory>

using namespace std;

int solver(bidiVector<rational_t>& Q, vector<rational_t> &b) {
    size_t N = b.size();

    vector<unique_ptr<vector<rational_t>>> QQ(N);
    
    // Copy data
    for(int i=0; i<N; i++) {
        QQ[i].reset(new vector<rational_t>(N));
        for(int j=0; j<N; j++)
            (*(QQ[i]))[j] = Q(i,j);
    }
    
    // Triangulate!
    for(int i=0; i<N-1; i++) {
        // Search for the first non-zero entry
        bool pivotFound = false;
        for(int j=i; j<N; j++)
            if( (*(QQ[j]))[i] != 0 ){
                // Here it is... swap!
                if(i != j){
                    swap(QQ[j], QQ[i]);
                    swap(b[j], b[i]);
                }
                pivotFound = true;
                break;
            }
        
        if(not pivotFound) // There is some under determination... report it
            return i;
        
        // Elimination downwards!
        auto& pivot = (*(QQ[i]))[i];
        // Normalize
        for(int j=i+1; j<N; j++)
            (*(QQ[i]))[j] /= pivot;
        b[i] /= pivot;
        (*(QQ[i]))[i] = 1;
        
        // Make the substractions corresponding to row j
        for(int j=i+1; j<N; j++) {
            auto& pivot = (*(QQ[j]))[i];
            if(pivot == 0) continue;
            for(int k=i+1; k<N; k++)
                (*(QQ[j]))[k] -= (*(QQ[i]))[k] * pivot;
            b[j] -= b[i] * pivot;
            (*(QQ[j]))[i] = 0;
        }
    }
    
    // The last one
    if((*(QQ[N-1]))[N-1] == 0) 
        // There is some under determination... report it
        return N-1;

    b[N-1] /= (*(QQ[N-1]))[N-1];
    (*(QQ[N-1]))[N-1] = 1;

    // After triangulating it... backwards substitution
    for(int i=N-1; i>0; i--)
        for(int j=i-1; j>= 0; j--)
            b[j] -= b[i] * (*(QQ[j]))[i];
    
    return -1;
}


