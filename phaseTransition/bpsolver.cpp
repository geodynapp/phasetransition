#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "bpsolver.h"

/*
 * A linear solver base on the Barreras & Peña LDU decompostition
 *
 *
 *
*/
void bpsolver( int n, real_t *A , real_t *b , real_t *r, real_t *X ){
    int i, j, k;
    int pi, pj, pk, pk1; //for permuted indices
    int x, y, z;
    
    // Memory allocation
    int* PER = (int*)malloc( sizeof(int) * n );       // Permutation
    real_t* L = (real_t*)malloc( sizeof(real_t) * n * n );
    real_t* U = (real_t*)malloc( sizeof(real_t) * n * n );
    real_t* D = (real_t*)malloc( sizeof(real_t) * n );
    real_t* s = (real_t*)malloc( sizeof(real_t) * n ); // Off-diagonal col sums
    real_t* h = (real_t*)malloc( sizeof(real_t) * n ); 
    
    
    // Initializations...
    for( i = 0 ; i < n ; i++ ){
        PER[i] = i; // Permutation
        s[i] = 0;
        x = n*i + i;
        h[i] = A[x];
        L[x] = 1;
        for( j = 0 ; j < n ; j++ ){
            x = n*i + j;
            U[x] = 0;
            if( j != i ){
                L[x] = 0;
                s[i] += A[n*j + i]; // Off-diagonal sol sums
            }
        } 
    }
    
    
    // Look for the first permutation. We're assuming that such element exists, 
    // since it is a diagonally dominant M-matrix!
    for( i = 0 ; i < n ; i++ ){
        if( h[i] >= -s[i] ){
            PER[0] = i;
            PER[i] = 0;
            break;
        }
    }
    
    // this should be done after
    D[0] = h[PER[0]];

    
    // Now, compute the LDU decomposition
    for( k = 0 ; k < n - 1 ; k++ ){
        pk = PER[k];
        if( D[k] == 0 ){
            for( i = k + 1 ; i < n ; i++ ){
                pi = PER[i];
                U[ n * pk + i ] = L[ n * pi + pk ] = 0;
            }
        } else {
            for( i = k + 1 ; i < n ; i++ ){
                pi = PER[i];
                x = n*pi + pk;
                y = n*pk + pi;
                z = n*pk + pk;
                
                L[x] = A[x] / A[z];
                U[y] = A[y] / A[z];
                r[pi] -= L[x] * r[pk];
                h[pi] -= U[y] * h[pk];
                s[pi] -= U[y] * s[pk];
                if( L[x] != 0.0 ){
                    for( j = k + 1 ; j < n ; j++ ){
                        if( i != j ){ // Equivalent to PER[i]!=PER[j]
                            pj = PER[j];
                            A[n*pi + pj] -= L[x] * A[n*pk + pj];
                        } 
                    }
                }
            }
        }
        
        //Compute the new pivot... that is a permutation
        for( i = k + 1 ; i < n ; i++ ){
            pi = PER[i];
            if(h[pi] >= -s[pi]){
                PER[i] = PER[k+1];
                PER[k+1] = pi;
                break;
            }
        }
        
        //Final steps
        pk1 = PER[k+1];
        D[k+1] = r[pk1];
        for( j = k + 2 ; j < n ; j++ )
            D[k+1] -= A[n*pk1 + PER[j]];
        A[n*pk1 + pk1] = D[k+1];
    }
    
    // Add the ones to the diagonal of U. This step can be dropped
    /*for(i=0;i<n;i++)
        U[n*PER[i] + PER[i]] = 1;*/
    
    
    // OK. Now, solve the system. If LDUx = b (with b reordered accordingly)
    // Let us start solving Ls=b with s = DUx. Since L has all entries less 
    // than 0 (except the diagonal) there is no substractions in the algorithm.
    for( i = 0 ; i < n ; i++ ){
        pi = PER[i];
        s[pi] = b[pi];
        for( j = 0 ; j < i ; j++ ){
            pj = PER[j];
            x = n*pi + pj;
            if( L[x] == 0.0 )
                continue;
            s[pi] -= L[x] * s[pj];
        }
    }
    
    // Now s = D s' with s' = Ux.
    for( i = 0 ; i < n ; i++ )
        s[PER[i]] /= D[i];
    
    // Finally, s = Ux so we can solve the system.
    for( i = n-1 ; i >= 0 ; i-- ){
        pi = PER[i];
        X[pi] = s[pi];
        for( j = n-1 ; j > i ; j-- ){
            pj = PER[j];
            x = n*pi + pj;
            if( U[x] == 0.0)
                continue;
            X[pi] -= U[x] * X[pj];
        }
    }
    
    free(PER);
    free(L);
    free(D);
    free(U);
    free(s);
    free(h);
}



